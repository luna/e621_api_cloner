# e621_api_cloner

use e621 db dumps to create your own e621 api server without needing to be ratelimited for too much scraping (this is a very specific use case i promise i am useful to society)

## how

```sh
python3 -m venv env
env/bin/pip install -Ur requirements.txt

# go to https://e621.net/db_export/ to find out the available db dumps
# database file available on e621.db
env/bin/python ./build_database.py 2022-08-27
env/bin/python ./e621_api_cloner.py ./e621.db
```
